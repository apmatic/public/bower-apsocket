'use strict';

angular.module('ap.socket', ['ng', 'btford.socket-io'])

  .factory('$socket', function (socketFactory, $q, $window, $rootScope, $timeout) {
    var socket = $q.defer();
    var options = {connect: '', opt: {query: 'token=' + $window.sessionStorage.token, forceNew: true, transports: ['websocket']}};

    $rootScope.$on('authenticated', function() {
      $timeout(function() {
        var newSocket = (function() {
          return socketFactory({
            ioSocket: io.connect(options.connect, options.opt)
          });
        })();

        socket.resolve(newSocket);
      });
    });

    return {
      ready: socket.promise,
      options: options
    }

  });
